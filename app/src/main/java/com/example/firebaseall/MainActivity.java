package com.example.firebaseall;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText edittextname;
    Spinner spinnergenres;
    //    this list will be paased in Adaptorclass object
    ArrayList<ArtistsModel> artistsList;
    Button buttonadd;
    RecyclerView recyclerView;
    //    firebase database reference
    DatabaseReference databaseReference;
    private static final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        artistsList = new ArrayList<>();

//      creating a database reference ;
        databaseReference = FirebaseDatabase.getInstance().getReference("NameOfDatabase");

        buttonadd = findViewById(R.id.addArtist);
        spinnergenres = findViewById(R.id.spinner);
        edittextname = findViewById(R.id.etname);


        buttonadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addArtist();
            }
        });
    }

    //          overide onStart() will show the list on starting the screen
    @Override
    protected void onStart() {

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//         artistsMOdelArraulist needs to be clear before calling otherwise it will bring the data + new data
                artistsList.clear();
//                this is how we fetch our data from firebase
                for (DataSnapshot artistSnapshot : dataSnapshot.getChildren()) {
                    ArtistsModel artistsModel = artistSnapshot.getValue(ArtistsModel.class);
                    artistsList.add(artistsModel);
                }
                AdaptorforRecylerview adaptorforRecylerview = new AdaptorforRecylerview(artistsList);
                recyclerView.setAdapter(adaptorforRecylerview);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        super.onStart();
    }

    public void addArtist() {
//      taking the data from edittest and spinner
        String name = edittextname.getText().toString();
        String genres = spinnergenres.getSelectedItem().toString();

        if (!TextUtils.isEmpty(name)) {

//      if name is not empty it will be saved to firebase from here
//      we need to make a unique id for out artist for that we use push mesthod:
//      this id will be passed as Id as in artistId in artist model class
            String id = databaseReference.push().getKey();
            ArtistsModel artistsModel = new ArtistsModel(id, name, genres);
//            now store this artistsmodel in firebase Database
//            we can not directly store the artistmodel so we need to put it the unique id that we created ID.
//            this will make a node of id name in firebase which will look odd to identify
//            databaseReference.child(artistsModel.getArtistName()).setValue(artistsModel);
//          this here will make a node of name of sartist name as getartistname
            databaseReference.child(artistsModel.getArtistName()).setValue(artistsModel);
//              i want the data to be added after on click not oncreate so ..
//              i created the recyclerview here
            artistsList.add(artistsModel);
            /*  AdaptorforRecylerview adaptorforRecylerview = new AdaptorforRecylerview(artistsList);
            Log.d(TAG, "addArtist: " + artistsList.size());
            recyclerView.setAdapter(adaptorforRecylerview); */
//            here if we pass the new value as above with same id it will be overwritten

            Toast.makeText(this, "artist added", Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(this, "Enter a name", Toast.LENGTH_SHORT).show();
        }
    }
}
