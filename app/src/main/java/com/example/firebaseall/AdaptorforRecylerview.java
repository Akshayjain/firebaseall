package com.example.firebaseall;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class AdaptorforRecylerview extends RecyclerView.Adapter<AdaptorforRecylerview.ProgrammingViewHolder> {

    public List<ArtistsModel> artistsModelslist;


//    this empty constructor is complesory
    public AdaptorforRecylerview() {
    }

    public AdaptorforRecylerview(List<ArtistsModel> artistsModelslist) {
        this.artistsModelslist = artistsModelslist;
    }

    @NonNull
    @Override
    public ProgrammingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.itemview, parent, false);
        return new ProgrammingViewHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull ProgrammingViewHolder holder, int position) {
//      making a object of the artistMOdel class and passing the position in it .
        ArtistsModel artistsModel = artistsModelslist.get(position);
        holder.artistname.setText(artistsModel.getArtistName());
        holder.artistgenre.setText(artistsModel.getArtistGenre());

    }

    @Override
    public int getItemCount() {
        return artistsModelslist.size();
    }

    public static class ProgrammingViewHolder extends RecyclerView.ViewHolder {

        TextView artistname;
        TextView artistgenre;


        public ProgrammingViewHolder(@NonNull View itemView) {
            super(itemView);

            artistname = itemView.findViewById(R.id.nameOfArtist);
            artistgenre = itemView.findViewById(R.id.genreOfArtist);


        }
    }
}
